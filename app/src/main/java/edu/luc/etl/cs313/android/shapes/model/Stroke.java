package edu.luc.etl.cs313.android.shapes.model;

/**
 * A decorator for specifying the stroke (foreground) color for drawing the
 * shape.
 */
public class Stroke implements Shape {
	private final int c;
	private final Shape s;

	public Stroke(final int color, final Shape shape) {
		this.c = color;
		this.s = shape;
	}

	public int getColor()
	{
		return c;
	}

	public Shape getShape()
	{
		return s;
	}

	@Override
	public <Result> Result accept(Visitor<Result> v)
	{
		return v.onStroke(this);
	}
}
