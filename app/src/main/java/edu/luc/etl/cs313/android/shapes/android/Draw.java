package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;
	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint)
	{
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c)
	{
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c)
	{
		int save = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(save);

		return null;
	}

	@Override
	public Void onFill(final Fill f)
	{
		Style save = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(save);
		return null;
	}

	@Override
	public Void onGroup(final Group g)
	{
		for(Shape s: g.getShapes())
		{
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l)
	{
		canvas.translate(l.getX(),l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(),-l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint); //Check getHeight & getWidth order later
		return null;
	}

	@Override
	public Void onOutline(Outline o)
	{
		Style save = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(save);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		final Point[] pts = new Point[s.getPoints().size()+1];
		final float[] coord = new float[(s.getPoints().size()+1)*4];
		int i, k=0;
		for (i=0; i < s.getPoints().size(); i++)
		{
			pts[i] = s.getPoints().get(i);
		}
		pts[i]=pts[0];;
		for(i=1; i < pts.length; i++)
		{
			coord[k] = pts[i-1].getX();
			coord[k+1] = pts[i-1].getY();
			coord[k+2] = pts[i].getX();
			coord[k+3] = pts[i].getY();
			k+=4;
		}
		canvas.drawLines(coord, paint);
		return null;
	}
}